# Fof.dsp

Fof.dsp is a collection of FAUST (faust.grame.fr) objects that work together to 
implement a filter--wavetable oscillator version of Formant-Wave-Function (FOF) 
synthesis. FOF is a vocal synthesis technique invented by Xavier Rodet at IRCAM 
in the early 1980s. The version of FOF contained herein accompanies the paper 
"A Hybrid Filter--Wavetable Oscillator technique for Formant-Wave-Function 
Synthesis" by Michael Jorgen Olsen, Julius O. Smith III and Jonathan S. Abel 
which was presented at the 2016 Sound and Music Computing Conference in 
Hamburg, Germany. For further description of the technique, please see the 
paper 
([here](http://quintetnet.hfmt-hamburg.de/SMC2016/wp-content/uploads/2016/09/SMC2016_proceedings.pdf#page=380))

---

To use this code, you need to either have FaustLive or the Faust compiler 
installed on your system (
[here](http://faust.grame.fr/download/)) or use the online 
Faust Compiler (
[here](http://faust.grame.fr/onlinecompiler/)).

---

This code is licensed with the Synthesis Tool Kit 4.3 license an MIT-style 
license (see LICENSE)

---

Comments, questions and suggestions can be directed to 
mjolsen@ccrma.stanford.edu
